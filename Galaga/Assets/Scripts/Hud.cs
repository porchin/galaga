﻿using Galaga.ObjectPools;
using TMPro;
using UnityEngine;

namespace Galaga
{
    public class Hud : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private string livePoolName;
        [SerializeField] private Transform liveContainer;
        [SerializeField] private GameObject gameOver;

        private int lives;

        public int Score
        {
            set
            {
                scoreText.text = $"SCORE: {value}";
            }
        }
        
        public int Lives
        {
            set
            {
                while (lives != value)
                {
                    if (lives < value)
                    {
                        ObjectPoolManager.Instance.GetPooledObject(livePoolName);
                        lives++;
                    }
                    else
                    {
                        liveContainer.GetChild(lives - 1).gameObject.SetActive(false);
                        lives--;
                    }
                }
            }
        }
    }
}