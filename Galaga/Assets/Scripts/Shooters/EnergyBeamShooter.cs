﻿using System.Collections;
using UnityEngine;

namespace Galaga.Shooters
{
    public class EnergyBeamShooter : BaseShooter
    {
        [SerializeField] private EnergyBeam beamPrefab;

        private EnergyBeam beam;
        
        private void OnDestroy()
        {
            if (beam != null)
            {
                Destroy(beam.gameObject);
            }
        }

        public override IEnumerator Shoot()
        {
            CreateEnergyBeam();
            yield break;
        }

        private void CreateEnergyBeam()
        {
            beam = Instantiate(beamPrefab, firePoint.position, firePoint.rotation);
            beam.tag = "EnemyBullet";

            foreach (Transform child in beam.transform)
            {
                child.tag = "EnemyBullet";
            }

            beam.onComplete.AddListener(EnergyBeamCompleteHandler);
        }

        private void EnergyBeamCompleteHandler()
        {
            shot.Invoke();
            Destroy(beam.gameObject);
        }
    }
}