﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Galaga.Shooters
{
    public class EnergyBeam : MonoBehaviour
    {
        [SerializeField] private Transform maskTransform;
        [SerializeField] private float releaseTime = 2f;
        [SerializeField] private float beamHeight = 0.6f;

        public UnityEvent onComplete = new UnityEvent();

        private void Awake()
        {
            var scale = maskTransform.localScale;
            scale.y = 0f;
            maskTransform.localScale = scale;
        }

        private IEnumerator Start()
        {
            yield return StartCoroutine(Enlarge());
            yield return StartCoroutine(Shrink());

            onComplete.Invoke();
        }

        private IEnumerator Enlarge()
        {
            var timer = 0f;

            while (maskTransform.localScale.y < beamHeight)
            {
                timer += Time.deltaTime / releaseTime;
                var scale = maskTransform.localScale;
                scale.y = Mathf.Lerp(0f, beamHeight, timer);
                maskTransform.localScale = scale;
                yield return null;
            }
        }

        private IEnumerator Shrink()
        {
            var timer = 0f;

            while (maskTransform.localScale.y > 0f)
            {
                timer += Time.deltaTime / releaseTime;
                var scale = maskTransform.localScale;
                scale.y = Mathf.Lerp(beamHeight, 0f, timer);
                maskTransform.localScale = scale;
                yield return null;
            }
        }
    }
}
