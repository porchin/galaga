﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Galaga.Shooters
{
    public abstract class BaseShooter : MonoBehaviour
    {
        [SerializeField] protected Transform firePoint;

        public UnityEvent shot = new UnityEvent();

        public abstract IEnumerator Shoot();
    }
}