﻿using System.Collections;
using UnityEngine;

namespace Galaga.Shooters
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private new Rigidbody2D rigidbody2D;
        [SerializeField] private float speed = 2f;
        [SerializeField] private float lifeTime = 5f;

        private void OnEnable()
        {
            rigidbody2D.velocity = transform.up * speed;
            StartCoroutine(WaitAndDestroy());
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (tag.Contains(other.tag))
            {
                return;
            }
            gameObject.SetActive(false);
        }

        private IEnumerator WaitAndDestroy()
        {
            yield return new WaitForSeconds(lifeTime);
            gameObject.SetActive(false);
        }
    }
}