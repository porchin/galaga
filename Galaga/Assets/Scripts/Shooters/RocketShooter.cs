﻿using Galaga.ObjectPools;
using System.Collections;
using UnityEngine;

namespace Galaga.Shooters
{
    public class RocketShooter : BaseShooter
    {
        [SerializeField] private int rocketCount;
        [SerializeField] private float intervalTime;
        [SerializeField] private string rocketPoolName;

        public override IEnumerator Shoot()
        {
            var waitForSeconds = new WaitForSeconds(intervalTime);
            var count = 0;
            while (count < rocketCount)
            {
                var rocket = ObjectPoolManager.Instance.GetPooledObject(rocketPoolName, firePoint.position, firePoint.rotation).transform;
                rocket.tag = "EnemyBullet";
                count++;

                yield return waitForSeconds;
            }
        }
    }
}

