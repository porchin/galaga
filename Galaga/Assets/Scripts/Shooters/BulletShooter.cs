﻿using Galaga.ObjectPools;
using System.Collections;
using UnityEngine;

namespace Galaga.Shooters
{
    public class BulletShooter : BaseShooter
    {
        [SerializeField] private string bulletPoolName;
        [SerializeField] private float fireRate = 1f;
        
        private bool canFire;

        private void OnEnable()
        {
            canFire = true;
        }

        private void OnDisable()
        {
            canFire = false;
        }

        public override IEnumerator Shoot()
        {
            if (!canFire)
            {
                yield break;
            }

            canFire = false;

            var bullet = ObjectPoolManager.Instance.GetPooledObject(bulletPoolName, firePoint.position, firePoint.rotation).transform;
            bullet.tag = "GalagaBullet";

            yield return new WaitForSeconds(1f / fireRate);
            canFire = true;
        }
    }
}
