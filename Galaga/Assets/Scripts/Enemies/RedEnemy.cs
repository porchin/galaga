﻿using Galaga.Shooters;
using UnityEngine;

namespace Galaga.Enemies
{
    public class RedEnemy : Enemy
    {
        [SerializeField] private BaseShooter shooter;
        [SerializeField] private float targetNearbyDistance = 2f;

        private Coroutine shootCoroutine;

        public override void StartAttacking()
        {
            if (!target.activeSelf)
            {
                return;
            }
            var position = target.transform.position;
            position.x = Random.Range(position.x - targetNearbyDistance, position.x + targetNearbyDistance);

            Direction = position - transform.position;
            movementState = EnemyMovementState.Attack;
            shootCoroutine = StartCoroutine(shooter.Shoot());
        }

        protected override void StartRegrouping()
        {
            base.StartRegrouping();
            StopCoroutine(shootCoroutine);
        }
    }
}