﻿namespace Galaga.Enemies
{
    public enum EnemyMovementState
    {
        Idle,
        Attack,
        Shoot,
        Regroup
    }
}
