﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Galaga.Enemies
{
    [Serializable]
    public class ScoreEvent : UnityEvent<int> { }

    [Serializable]
    public class EnemyEvent : UnityEvent<Enemy> { }

    public abstract class Enemy : MonoBehaviour
    {
        protected const float minDistanceThreshold = 0.04f;

        [SerializeField] private float speed = 10f;
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private int score = 100;

        public ScoreEvent onScoreGranting = new ScoreEvent();
        public EnemyEvent onEnemyDestroyed = new EnemyEvent();
        public EnemyEvent onEnemyRegrouped = new EnemyEvent();

        [HideInInspector] public GameObject target;
        
        protected Vector3 startPosition;
        protected Vector3 startEulerAngles;
        protected EnemyMovementState movementState;

        private Vector3 direction;
        private float bound;
        private float spawnTime;

        protected Vector3 Direction
        {
            get
            {
                return direction;
            }
            set
            {
                direction = value;
                transform.rotation = Quaternion.LookRotation(Vector3.forward, direction);
            }
        }

        private void Awake()
        {
            bound = Camera.main.orthographicSize + (spriteRenderer.sprite.rect.height * transform.localScale.y / (2f * spriteRenderer.sprite.pixelsPerUnit));
            startPosition = transform.position;
            startEulerAngles = transform.eulerAngles;
            movementState = EnemyMovementState.Idle;
            spawnTime = Time.fixedTime;
        }

        private void FixedUpdate()
        {
            switch (movementState)
            {
                case EnemyMovementState.Idle:
                    Idle();
                    break;
                case EnemyMovementState.Attack:
                    Attack();
                    break;
                case EnemyMovementState.Shoot:
                    Shoot();
                    break;
                case EnemyMovementState.Regroup:
                    Regroup();
                    break;
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Galaga") && !other.CompareTag("GalagaBullet"))
            {
                return;
            }

            var scoreToGrant = other.CompareTag("GalagaBullet") ? score : 0;

            onScoreGranting.Invoke(scoreToGrant);
            onEnemyDestroyed.Invoke(this);
            Destroy(gameObject);
        }

        public virtual void StartAttacking() { }

        protected virtual void Idle()
        {
            var position = transform.position;
            position.x = startPosition.x + (Mathf.Sin((Time.fixedTime - spawnTime) % (2 * Mathf.PI)) / 2f);
            transform.position = position;
        }

        protected virtual void Attack()
        {
            if (!target.activeSelf)
            {
                StartRegrouping();
                return;
            }

            if (transform.position.y < -bound)
            {
                transform.position = new Vector3(transform.position.x, bound, transform.position.z);
                StartRegrouping();
                return;
            }

            transform.Translate(Direction.normalized * speed * Time.deltaTime, Space.World);
        }

        protected virtual void StartShooting() { }
        protected virtual void Shoot() { }

        protected virtual void StartRegrouping()
        {
            movementState = EnemyMovementState.Regroup;
        }

        private void Regroup()
        {
            var position = startPosition;
            position.x += Mathf.Sin((Time.fixedTime - spawnTime) % (2 * Mathf.PI)) / 2f;

            if (Vector3.Distance(transform.position, position) < minDistanceThreshold)
            {
                onEnemyRegrouped.Invoke(this);
                movementState = EnemyMovementState.Idle;
                transform.eulerAngles = startEulerAngles;
                return;
            }

            Direction = position - transform.position;
            transform.Translate(Direction.normalized * speed * Time.deltaTime, Space.World);
        }
    }
}
