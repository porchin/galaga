﻿using Galaga.Shooters;
using UnityEngine;

namespace Galaga.Enemies
{
    public class GreenEnemy : Enemy
    {
        [SerializeField] private BaseShooter shooter;
        [SerializeField] private float distanceToShoot = 2f;

        private Coroutine shootCoroutine;
        private Vector3 targetPosition;

        public override void StartAttacking()
        {
            if (!target.activeSelf)
            {
                return;
            }
            targetPosition = target.transform.position;
            targetPosition.y += distanceToShoot;
            Direction = targetPosition - transform.position;
            movementState = EnemyMovementState.Attack;
        }

        protected override void Attack()
        {
            base.Attack();
            if (Vector3.Distance(transform.position, targetPosition) < minDistanceThreshold)
            {
                StartShooting();
            }
        }

        protected override void StartShooting()
        {
            transform.eulerAngles = startEulerAngles;

            shooter.shot.AddListener(ShotHandler);
            shootCoroutine = StartCoroutine(shooter.Shoot());

            movementState = EnemyMovementState.Shoot;
        }

        protected override void StartRegrouping()
        {
            base.StartRegrouping();
            if (shootCoroutine != null)
            {
                StopCoroutine(shootCoroutine);
            }
        }

        private void ShotHandler()
        {
            movementState = EnemyMovementState.Regroup;
        }
    }
}