﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Galaga.Enemies
{
    public class EnemyForce : MonoBehaviour
    {
        [SerializeField] private float intervalTime = 1f;
        public List<Enemy> enemies;

        public UnityEvent onEnemyForceDestroyed = new UnityEvent();

        [HideInInspector] public GameObject target;
        private readonly List<Enemy> idleEnemies = new List<Enemy>();
        private readonly List<Enemy> attackingEnemies = new List<Enemy>();
        private float timer;

        public bool IsInPosition { get { return attackingEnemies.Count == 0; } }
        
        private void Start()
        {
            idleEnemies.AddRange(enemies);

            foreach (var enemy in enemies)
            {
                enemy.target = target;
                enemy.onEnemyDestroyed.AddListener(EnemyDestroyedHandler);
                enemy.onEnemyRegrouped.AddListener(EnemyRegroupedHandler);
            }
        }

        private void Update()
        {
            if (!target.activeSelf)
            {
                return;
            }

            if (timer >= intervalTime)
            {
                timer = 0f;

                if (idleEnemies.Count > 0)
                {
                    Attack();
                }
            }

            timer += Time.deltaTime;
        }

        private void Attack()
        {
            var index = Random.Range(0, idleEnemies.Count);
            var enemy = idleEnemies[index];
            enemy.StartAttacking();
            attackingEnemies.Add(enemy);
            idleEnemies.Remove(enemy);
        }

        private void EnemyDestroyedHandler(Enemy enemy)
        {
            attackingEnemies.Remove(enemy);
            idleEnemies.Remove(enemy);
            enemies.Remove(enemy);

            if (enemies.Count == 0)
            {
                onEnemyForceDestroyed.Invoke();
                Destroy(gameObject);
            }
        }

        private void EnemyRegroupedHandler(Enemy enemy)
        {
            idleEnemies.Add(enemy);
            attackingEnemies.Remove(enemy);
        }
    }
}