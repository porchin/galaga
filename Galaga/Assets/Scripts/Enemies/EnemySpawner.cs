﻿using UnityEngine;

namespace Galaga.Enemies
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] private EnemyForce[] enemyForcePrefabs;

        public EnemyForce Spawn(GameController gameController, int index)
        {
            return Instantiate(enemyForcePrefabs[index], transform);
        }
    }
}
