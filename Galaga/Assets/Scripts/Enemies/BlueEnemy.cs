﻿using UnityEngine;

namespace Galaga.Enemies
{
    public class BlueEnemy : Enemy
    {
        public override void StartAttacking()
        {
            if (!target.activeSelf)
            {
                return;
            }

            Direction = target.transform.position - transform.position;
            transform.rotation = Quaternion.LookRotation(Vector3.forward, Direction);

            movementState = EnemyMovementState.Attack;
        }
    }
}