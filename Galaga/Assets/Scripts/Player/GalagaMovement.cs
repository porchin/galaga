﻿using UnityEngine;

namespace Galaga.Players
{
    public class GalagaMovement : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private float speed = 10f;
        
        private float bound;
        private float horizontalInput;

        private void Awake()
        {
            bound = (Camera.main.aspect * Camera.main.orthographicSize) - (spriteRenderer.sprite.rect.width * transform.localScale.x / (2f * spriteRenderer.sprite.pixelsPerUnit));
        }

        private void Update()
        {
            horizontalInput = Input.GetAxisRaw("Horizontal");
        }

        private void FixedUpdate()
        {
            Move();
        }

        private void Move()
        {
            var newPosition = transform.position;
            newPosition.x = Mathf.Clamp(transform.position.x + horizontalInput * speed * Time.fixedDeltaTime, -bound, bound);
            transform.position = newPosition;
        }
    }
}