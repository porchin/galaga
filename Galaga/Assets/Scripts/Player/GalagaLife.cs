﻿using UnityEngine;
using UnityEngine.Events;

namespace Galaga.Players
{
    public class GalagaLife : MonoBehaviour
    {
        public UnityEvent hit = new UnityEvent();

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Enemy") && !other.CompareTag("EnemyBullet"))
            {
                return;
            }

            gameObject.SetActive(false);
            hit.Invoke();
        }
    }
}
