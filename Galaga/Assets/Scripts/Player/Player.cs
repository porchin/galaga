﻿using Galaga.Shooters;
using System;
using UnityEngine;
using UnityEngine.Events;

namespace Galaga.Players
{
    [Serializable]
    public class LivesValueChangedEvent : UnityEvent<int> { }
    
    [Serializable]
    public class ScoreChangedEvent : UnityEvent<int> { }

    public class Player : MonoBehaviour
    {
        public GameObject galaga;
        [SerializeField] private BaseShooter galagaShooter;

        public LivesValueChangedEvent onLivesValueChanged = new LivesValueChangedEvent();
        public UnityEvent onLivesReachedZero = new UnityEvent();
        public ScoreChangedEvent onScoreChanged = new ScoreChangedEvent();
        public UnityEvent onGalagaRespawning = new UnityEvent();
        public UnityEvent onGalagaRespawned = new UnityEvent();
        
        private int lives;
        private int score;

        public int Score
        {
            get
            {
                return score;
            }
            set
            {
                score = value;
                onScoreChanged.Invoke(score);
            }
        }

        public int Lives
        {
            get
            {
                return lives;
            }
            set
            {
                lives = value;
                onLivesValueChanged.Invoke(lives);
                if (lives <= 0)
                {
                    onLivesReachedZero.Invoke();
                }
            }
        }

        private void Update()
        {
            if (Input.GetButtonDown("Fire"))
            {
                StartCoroutine(galagaShooter.Shoot());
            }
        }

        public void Setup(int startLives)
        {
            Lives = startLives;
            Score = 0;
        }

        public void ReduceLive()
        {
            Lives--;
            if (Lives > 0)
            {
                onGalagaRespawning.Invoke();
            }
        }

        public void SpawnGalaga()
        {
            galaga.transform.localPosition = Vector3.zero;
            galaga.SetActive(true);
            onGalagaRespawned.Invoke();
        }
    }
}