﻿using System.Collections.Generic;
using UnityEngine;

namespace Galaga.ObjectPools
{
    public class ObjectPoolManager : MonoBehaviour
    {
        public List<ObjectPool> objectPools = new List<ObjectPool>();

        public static ObjectPoolManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }
            Instance = this;

            foreach (var objectPool in objectPools)
            {
                InstantiateObjectsToPool(objectPool);
            }
        }

        public GameObject GetPooledObject(string poolName)
        {
            return GetPooledObject(poolName, Vector3.zero, Quaternion.identity);
        }

        public GameObject GetPooledObject(string poolName, Vector3 position, Quaternion rotation)
        {
            var objectPool = objectPools.Find(x => x.name == poolName);
            if (objectPool == null)
            {
                return null;
            }
            
            var objectToSpawn = objectPool.gameObjects.Find(x => !x.activeSelf);
            if (objectToSpawn == null)
            {
                objectToSpawn = InstantiateObjectToPool(objectPool);
            }

            objectToSpawn.transform.position = position;
            objectToSpawn.transform.rotation = rotation;
            objectToSpawn.SetActive(true);

            return objectToSpawn;
        }

        private void InstantiateObjectsToPool(ObjectPool objectPool)
        {
            for (var i = 0; i < objectPool.initialSize; i++)
            {
                InstantiateObjectToPool(objectPool);
            }
        }

        private GameObject InstantiateObjectToPool(ObjectPool objectPool)
        {
            var pooledObject = Instantiate(objectPool.prefab, objectPool.parent);
            pooledObject.SetActive(false);
            objectPool.gameObjects.Add(pooledObject);

            return pooledObject;
        }
    }
}

