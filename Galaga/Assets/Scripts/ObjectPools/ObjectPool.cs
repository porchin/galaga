﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Galaga.ObjectPools
{
    [Serializable]
    public class ObjectPool
    {
        public string name;
        public GameObject prefab;
        public Transform parent;
        public int initialSize;
        public List<GameObject> gameObjects = new List<GameObject>();
    }
}

