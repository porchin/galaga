﻿using Galaga.Enemies;
using Galaga.Players;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Galaga
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private float startDelayTime = 2f;
        [SerializeField] private Player player;
        [SerializeField] private int startLives;
        [SerializeField] private EnemySpawner enemySpawner;

        public UnityEvent onGameStarting = new UnityEvent();
        public UnityEvent onGameStarted = new UnityEvent();

        private EnemyForce enemyForce;

        private IEnumerator Start()
        {
            player.Setup(startLives);
            if (enemyForce != null)
            {
                Destroy(enemyForce.gameObject);
            }
            enabled = false;

            onGameStarting.Invoke();

            yield return new WaitForSeconds(startDelayTime);
            
            player.SpawnGalaga();
            SpawnEnemyForce();
            
            onGameStarted.Invoke();
        }

        private void Update()
        {
            if (Input.GetButtonDown("Fire"))
            {
                StartCoroutine(Start());
            }
        }

        public void GrantScore(int scoreToGrant)
        {
            player.Score += scoreToGrant;
        }

        public void RespawnGalaga()
        {
            StartCoroutine(SpawnGalaga());
        }

        public void EndGame()
        {
            enabled = true;
        }

        private void SpawnEnemyForce()
        {
            enemyForce = enemySpawner.Spawn(this, 0);
            enemyForce.target = player.galaga;
            enemyForce.onEnemyForceDestroyed.AddListener(EnemyForceDestroyedHandler);

            foreach (var enemy in enemyForce.enemies)
            {
                enemy.onScoreGranting.AddListener(GrantScore);
            }
        }

        private IEnumerator SpawnGalaga()
        {
            if (player.Lives == 0)
            {
                yield break;
            }
            
            yield return new WaitForSeconds(startDelayTime);

            while (!enemyForce.IsInPosition)
            {
                yield return null;
            }
            
            player.SpawnGalaga();
        }

        private void EnemyForceDestroyedHandler()
        {
            SpawnEnemyForce();
        }
    }
}